/*
Part of the Cake Orchestra project http://pindec.co.uk/cake-orchestra
MIT License
Copyright (c) 2017 Charlie Pinder
*/

import arb.soundcipher.*;
import arb.soundcipher.constants.*;

import processing.serial.*;

import arb.soundcipher.*;
import arb.soundcipher.constants.*;


// our SoundCipher objects
SoundCipher sc = new SoundCipher(this);
SoundCipher rfidPlayer = new SoundCipher(this);

// An list of the RFID tags we've received recently 
ArrayList<String> receivedIDs = new ArrayList<String>();

// a list of RFID tag id numberss and their related id
HashMap<String, Integer> buttons = new HashMap<String, Integer>();

// a way to decribe the current instrument
String currentInstrument;

// a list of our display cakes
ArrayList<Cake> cakes = new ArrayList<Cake>();
PImage cakeImage;

// our play flag
boolean masterPlay = true;

// our variables we need to listen to infomation arriving from the Arduino
private Serial myPort;
private String readString;

// our instrument counter, starts at minus 1 because we increment it first, and arrays indices start at zero :).
int instrumentCount = -1;

// refresh rate
int speed = 3;

// our delimiters
private String rfidDelim = "Tag";

// initial instrument set up
// for our RFID tags
float instruments[] = {sc.SPACE_VOICE, sc.POLYSYNTH, sc.HAND_CLAP, sc.MARIMBA, sc.SITAR, sc.CHOIR, sc.TAIKO, sc.BREATHNOISE, sc.SEASHORE, sc.TELEPHONE};
String instrumentNames[] = {"SPACE VOICE", "POLYSYNTH", "HANDCLAP", "MARIMBA", "SITAR", "CHOIR", "TAIKO", "BREATHNOISE", "SEASHORE", "TELEPHONE"};
int instrumentCounter = -1;
// and do buttons.put for each tag -> tags index mapping
// RFID tag ids
// replace these strings with your own IDs
// ID 1 is the control - it shifts the instruments
buttons.put("7C0056417912", 1);
// all the other tags play a note based on its ID number
buttons.put("77009700B959",2);
buttons.put("7C0056088BA9",3);
buttons.put("7C0055FDCA1E",4);
buttons.put("7C0056365844",5);
buttons.put("770096F12535",6);
buttons.put("7C0056400C66",7);
buttons.put("7700972C3EF2",8);
buttons.put("7700971619EF",9);
buttons.put("7C0055DF35C3",10);

void setup()
{

size(500,600);
  frameRate(speed);  
  // specific port to talk to 
  String portName = "COM5";
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');
  
  masterPlay= true;
  
  cakeImage = loadImage("cake.png");

  background(250);
  
  // start with a viola because violas sound nice :)
  rfidPlayer.instrument = rfidPlayer.VIOLA;
  currentInstrument = "VIOLA";

  registerMethod("pre", this);

}


void draw()
{
  // clear
  background(250);
  
  // do instruments
  playSounds();
  
  // do drawing
  write();
  
  for(Cake c: cakes)
  {  
   c.move();
   c.display();
   
   // remove cakes that have moved off the screen
   if(c.offscreen)
   {
     cakes.remove(c);
   }
  }
}

void pre()
{
}


/**
 A method that plays sounds based on RFID tags presented.
 If the tag ID is 1, then instruments will be switched
**/
void playSounds()
{

if(masterPlay)
  {
    
    if(receivedIDs.size() > 0) // if any RFID tags have been presented
      {
        println("playing ids");
         for(String id : receivedIDs)
        {
          // use map from id to float
          if(id != null && buttons != null && buttons.get(id) != null)
          {
              
              int tagNo = buttons.get(id);
              println("playing tag " + tagNo);
              
              if(tagNo == 1) // a control tag
              {
                // flip the instrument
                instrumentCounter++;
                if(instrumentCounter >= instruments.length) instrumentCounter = 0; // reset if neccessary.
                rfidPlayer.instrument = instruments[instrumentCounter];
                currentInstrument = instrumentNames[instrumentCounter];
                background(0,255,0); // flash
              }
          
          // 
            else
            {
              // play a note 
              rfidPlayer.playNote(tagNo * 10, random(70,130), 2);
            
              // create a matching cake
              cakes.add(new Cake(cakeImage, tagNo));
              break;
            }
          }
        } 
        // reset ids 
      receivedIDs.clear();
      }
  
  }
}

void keyPressed()
{
  if(key == 't')
  {
    instrumentCount++;
  }
  
  if(key == 'r')
  {
    //receivedIDs.add("test");
  }
   
  if(key == '!') stopStart(); 
 
}

void serialEvent(Serial thisPort)
{
  
  //get the temp from the serial port
    while (myPort.available() > 0) 
    {
      // get everything up to the linebreak
        String readStringUntil = myPort.readStringUntil('\n');
        if(readStringUntil == null)
        break;
  
      readString = readStringUntil.trim();
      if(readString!=null)
      {
         if(readString.startsWith(rfidDelim))
        {
          readString = readString.substring(6, readString.length()-1);
          // need to find out!
          //println("got id: " + readString);
          if(readString != null && readString.startsWith("7"))
          {
            if(!receivedIDs.contains(readString))
              receivedIDs.add(readString); // unless it's toggle instruments! but no toggle for rfid ones yet.
          }
        }
      
      else
        println("not sure what string this is: " + readString);

    }
    
    else
      println("read string is null");
    }
}

void stopStart()
{
  masterPlay = !masterPlay;
}

void drawCake(int drawMe)
{
  // not needed here - see Cake object
}

void write()
{
  if(currentInstrument != null)
  {
    textSize(30);
    fill(0);
    text(currentInstrument, 40,40);
  }
}
