/*
Part of the Cake Orchestra project http://pindec.co.uk/cake-orchestra
MIT License
Copyright (c) 2017 Charlie Pinder
*/

class Cake
{
    // a class to draw cake images
    int size;
    // position variables for drawing
    float xpos, ypos;
    // 'speed' - so how much the position variables change
    float xspeed, yspeed;
    // left/right/up/down
    float xdirection = 1;
    float ydirection = 1;
    
    PImage drawMe;
    int tagNo;
    
    boolean offscreen = false; // set if this object is no longer visible

    // Constructor method to make new Cale objects . 
    // Note how the class variables (defined above) are set 
    // by adding arguments to the constructor
    Cake(PImage c, int tag)
    {
        size = 50;
        // positioning stuff. We have x and y in the constructor so we can make objects appear where the user clicks
        xpos = random(0,800);  
        ypos = 0;  
       // random speed y values
        yspeed = random(25.0, 49.0);
        xdirection = 0;
        ydirection = 1;
        drawMe = c;
        tagNo = tag;
    }

    // A method to draws a cake in a given position
    void display()
    {
      if(ypos > (height + drawMe.height)) // if this cake has gone outside window bounds, i.e. y is greater than window height plus our image height
      {
        // set offscreen to true
        offscreen = true;
        return; // don't draw if off the screen
      }
      // otherwise draw in position
      image(drawMe, xpos, ypos, size, size);
      fill(255,0,0);
      textSize(18);
      text(tagNo, xpos + size / 2, ypos + size/2);
    }

    // A move method that shifts the cakes around the screen
    void move()
    {
        // adjust our y positions
        ypos +=  yspeed;
    }
} //end of stuff that belongs to cake class
