/*
Part of the Cake Orchestra project http://pindec.co.uk/cake-orchestra
MIT License
Copyright (c) 2017 Charlie Pinder
*/

#include <SoftwareSerial.h>


SoftwareSerial rfid (2, 3);
String msg;
String ID ;  //string to store allowed cards

void setup()  
{
  Serial.begin(9600);
  Serial.println("Serial Ready");

  rfid.begin(9600);
  Serial.println("RFID Ready");
}

char c;
void loop(){
  while(msg.length()<13)
  {
    while(rfid.available()>0)
    {
      c=rfid.read(); 
      msg += c;
    }
  }
  msg=msg.substring(1,13);
  Serial.println("ID" + msg);  //Uncomment to view your tag ID
    //Serial.println(msg.length());
    msg = "";
}

