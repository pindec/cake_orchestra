# README #

This repo contains the resources to help you build your own [Cake Orchestra](http://pindec.co.uk/cake-orchestra), RFID version

## Pre-requisites ##

* An Arduino
* An RFID reader and RFID tags attached to your Arduino. 
* A computer with [Arduino](https://www.arduino.cc/) and [Processing](https://processing.org/) software installed
* Install the Processing [Sound Cipher library](http://explodingart.com/soundcipher/download.html)

## Set up ##

* Summary:
	* Deploy to your Arduino
	* Add the Processing sketch to your local Processing library
	* Do the config
	* Run the Processing sketch
* Configuration
	* Declare your RFID IDs from _buttons.put("7C0056417912", 1);_ lines (string RFID id, int button ID). The tag you define with ID 1 is a control tag that switches the current instrument.
	* Set correct port for your Arduino on line 70 of cake_orchestra_RFID

## Running ##
* Present an RFID tag. If the tag is not ID 1, then a sound should play and a cake should appear on screen (displaying instrument name and tag number).
* To switch instruments, either present the contorl tag (ID = 1), or press "t" on the keyboard
* To kill the sounds, press "!". Same button toggles sounds back on.

### Help! ###

* Contact [over here](http://pindec.co.uk/about/) 

### Coding extensions you can try ###

The cake orchestra is intended to interest people in learning to code in creative ways.

You could try:

* editing the cake move() method to make the cakes move around in more interesting ways. What about trying to bounce them?
* edit the cake_orchestra_RFID class _cakeImage = loadImage("cake.png");_ line to show a different image. 
	* What about a random image for each cake? Or try flying pigs? Can you animate the image rather than just using a static one (see [here](http://csbham.sketchpad.cc/sp/pad/view/xCSiFr5a76/latest) for code hints)? 